package hu.elte.msc.webtech.tests;

import hu.elte.msc.webtech.dao.UserDao;
import hu.elte.msc.webtech.entity.Cours;
import hu.elte.msc.webtech.entity.Financial;
import hu.elte.msc.webtech.entity.Personal;
import hu.elte.msc.webtech.entity.User;
import hu.elte.msc.webtech.service.RegistrationService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/application-base.xml" })
public class RegistrationTest{
	
	@Autowired
    private RegistrationService regService;
	@Autowired
	private UserDao userDao;
	
	private static User user = new User();
	
	@Configuration
    @ComponentScan(basePackages = {"hu.elte.msc.webtech"})
    static class ContextConfiguration {} 
	
	@Test
    public void registerUser() throws Exception
    {
		//Create user
		user.setUsername("csandi");
		user.setPassword("csandi");
		user.setEnabled((byte)(0x01));
		user = regService.initUser(user);
		
		//Add personal info
		Personal personal = new Personal();
		personal.setAddress("1118 Budapest, Ménesi út 11-13");
		personal.setBirthDate(new Date());
		personal.setEha("CSATABI");
		personal.setEmail("test@test.hu");
		personal.setIdNumber("032145LA");
		personal.setMothersName("Winch Eszter");
		personal.setName("Mokk Béla");
		
		//Add Finacial
		Financial financial = new Financial();
		financial.setAccNumber("11742154-00315452-00000000");
		financial.setVatNumber("15308744-2-41");
		
		user.setPersonal(personal);
		personal.setUser(user);
		
		user.setFinancial(financial);
		financial.setUser(user);
		
		regService.save(user);
		System.out.println(regService);
    }
	
	@Test
    public void addCourse() throws Exception
    {	
		List<Cours> lst = new ArrayList<Cours>();
		
		Cours course = new Cours();
		course.setCode("xxxN1");
		course.setCredit(new BigDecimal(2));
		course.setDescription("Leírás");
		course.setName("Test Tárgy");
		
		lst.add(course);
		
		user.setCourses(lst);
		regService.merge(user);
    }
	
	@Test
	@Transactional
    public void testDao() throws Exception
    {	
		List<Cours> lst = userDao.getCoursesPaginated(1, 10);
		
		for (Cours cours : lst) {
			System.out.println(cours.toString());
		}
		
    }
}
