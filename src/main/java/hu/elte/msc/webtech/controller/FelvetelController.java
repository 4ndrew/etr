package hu.elte.msc.webtech.controller;

import hu.elte.msc.webtech.entity.User;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FelvetelController {
	
	@RequestMapping(value = "/felvetel.html", method = RequestMethod.GET)
	public void getTanulo(Model model) {
		User tanulo = new User();
		model.addAttribute("tanulo", tanulo);
 	}
}

