package hu.elte.msc.webtech.controller;

import hu.elte.msc.webtech.dao.UserDao;
import hu.elte.msc.webtech.entity.User;
import hu.elte.msc.webtech.security.SecurityUtil;
import hu.elte.msc.webtech.service.PersonalService;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WelcomeController {
	

	@Autowired
	private UserDao userDao;
	@Autowired
	PersonalService personalService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

	@RequestMapping(value = "/welcome.html", method = RequestMethod.GET)
	public void getTanulok(Model model,Principal principal) {
		
		String userName = principal.getName();
		
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		boolean roleAdmin = SecurityUtil.isAdmin(auth);
		
		List<User> results = (List<User>) userDao.getUsers();
		model.addAttribute("tanulok", results);
		model.addAttribute("userName", userName);
		model.addAttribute("roleAdmin", roleAdmin);
	}
}
