package hu.elte.msc.webtech.controller;

import java.util.List;

import hu.elte.msc.webtech.entity.Cours;
import hu.elte.msc.webtech.service.GuestService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class GuestController {
	
	@Autowired
	GuestService guestService;
	
	@RequestMapping(value = "/guest.html", method = RequestMethod.GET)
	public void guestPage(Model model) {
		
		List<Cours> courses =  guestService.getCourses(1, 10);
		model.addAttribute("courses", courses);
	}
}
