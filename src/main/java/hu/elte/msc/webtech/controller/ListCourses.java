package hu.elte.msc.webtech.controller;

import hu.elte.msc.webtech.entity.Cours;
import hu.elte.msc.webtech.security.SecurityUtil;
import hu.elte.msc.webtech.service.GuestService;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ListCourses {
	
	@Autowired
	GuestService guestService;
	
	@RequestMapping(value = "/listCourses.html", method = RequestMethod.GET)
	public void pesonalSubPage(Model model, Principal principal) {
		
		String userName = principal.getName();
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		boolean roleAdmin = SecurityUtil.isAdmin(auth);
		
		List<Cours> courses =  guestService.getCourses(1, 10);
		model.addAttribute("courses", courses);
		model.addAttribute("userName", userName);
		model.addAttribute("roleAdmin", roleAdmin);
	}
}