package hu.elte.msc.webtech.controller;

import hu.elte.msc.webtech.security.SecurityUtil;

import java.security.Principal;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CoursesSignUp {
	
	@RequestMapping(value = "/courseSignUp.html", method = RequestMethod.GET)
	public void pesonalSubPage(Model model, Principal principal) {
		
		String userName = principal.getName();
		
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		boolean roleAdmin = SecurityUtil.isAdmin(auth);
		
		model.addAttribute("userName", userName);
		model.addAttribute("roleAdmin", roleAdmin);
	}
}
