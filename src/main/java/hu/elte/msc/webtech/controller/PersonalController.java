package hu.elte.msc.webtech.controller;

import hu.elte.msc.webtech.entity.User;
import hu.elte.msc.webtech.security.SecurityUtil;
import hu.elte.msc.webtech.service.PersonalService;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PersonalController {
	
	@Autowired
	private PersonalService personalService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
	
	@RequestMapping(value = "/personal.html", method = RequestMethod.GET)
	public String pesonalSubPage(@RequestParam(value = "username", required = false) String selected, Model model, Principal principal, HttpServletRequest request) {
		
		User user;
		String userName = principal.getName();
		
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		boolean roleAdmin = SecurityUtil.isAdmin(auth);
		
		if(selected != null && !"".equals(selected) && roleAdmin){
			user = personalService.getSelectedUser(selected);
			if(user == null){
				return "redirect:personal.html?username=";
			}
		}else{
			user = personalService.getUserByUsername(userName);
		}
		
		if(roleAdmin){
			Map<String, String> users = personalService.getUsersForSelect();
			
			model.addAttribute("users", users);
		}
		
		model.addAttribute("user", user);
		model.addAttribute("readOnly", !roleAdmin);
		model.addAttribute("userName", userName);
		model.addAttribute("roleAdmin", roleAdmin);
		
		return "personal";
	}
	
	@RequestMapping(value = "/personal.html", method = RequestMethod.POST)
	public void update(@ModelAttribute User tanulo, Model model, Principal principal) {
		
		String userName = principal.getName();
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		boolean roleAdmin = SecurityUtil.isAdmin(auth);
		
		if(roleAdmin){
			Map<String, String> users = personalService.getUsersForSelect();
			
			model.addAttribute("users", users);
		}
		
		if (tanulo != null) {
	            Logger.getAnonymousLogger().log(Level.INFO, "Student NOT null: ");
	            Logger.getAnonymousLogger().log(Level.INFO, tanulo.getPersonal().getName());
	            
	            
	            User persistentUser = personalService.getUserByUsername(tanulo.getUsername());
	            
	            //Personal
	            persistentUser.getPersonal().setName(tanulo.getPersonal().getName());
	            persistentUser.getPersonal().setEha(tanulo.getPersonal().getEha());
	            persistentUser.getPersonal().setEmail(tanulo.getPersonal().getEmail());
	            persistentUser.getPersonal().setBirthDate(tanulo.getPersonal().getBirthDate());
	            persistentUser.getPersonal().setMothersName(tanulo.getPersonal().getMothersName());
	            persistentUser.getPersonal().setIdNumber(tanulo.getPersonal().getIdNumber());
	            
	            //Financial
	            persistentUser.getFinancial().setAccNumber(tanulo.getFinancial().getAccNumber());
	            persistentUser.getFinancial().setVatNumber(tanulo.getFinancial().getAccNumber());
	            
	            try {
	            	personalService.mergeUser(persistentUser);
	            	
	            } catch (Exception ex) {
	                ex.printStackTrace();
	            }

	        } else {
	            Logger.getAnonymousLogger().log(Level.WARNING, "Student null: ");
	        }
		 
		 model.addAttribute("userName", userName);
		 model.addAttribute("roleAdmin", roleAdmin);
		 model.addAttribute("readOnly", !roleAdmin);
	}
	
	
}
