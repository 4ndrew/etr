package hu.elte.msc.webtech.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class SecurityUtil {
	
	public static boolean isAdmin(Authentication auth){
		for (GrantedAuthority authority : auth.getAuthorities()) {
			System.out.println(authority.getAuthority());
			if("ROLE_ADMIN".equals(authority.getAuthority())){
				return true;
			}
		}
		return false;
	}

}
