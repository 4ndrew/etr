package hu.elte.msc.webtech.security;

import hu.elte.msc.webtech.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public class DatabasePasswordSecurerBean extends JdbcDaoSupport {
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private SaltSource saltSource;
	@Autowired
	private UserDetailsService userDetailsService;
	
	public void secureDatabase() { 
		getJdbcTemplate().query("select username, password from users", new RowCallbackHandler(){
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				String username = rs.getString(1);
				String password = rs.getString(2);
				UserDetails user = userDetailsService.loadUserByUsername(username); 
				String encodedPassword = passwordEncoder.encodePassword(password, saltSource.getSalt(user));
				getJdbcTemplate().update("update users set password = ? where username = ?", 
						encodedPassword, 
						username);
				System.out.println("Updating password for username: "+username+" to: "+encodedPassword);
			}			
		});
	}
	
	public User secureUser(final User user) { 

		String username = user.getUsername();
		String password = user.getPassword();
		
		UserDetails userDet = userDetailsService.loadUserByUsername(username); 
		String encodedPassword = passwordEncoder.encodePassword(password, saltSource.getSalt(userDet));
		user.setPassword(encodedPassword);

		return user;
	}
	
	public UserDetailsService getUserDetailsService() {
		return userDetailsService;
	}

	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	public PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public SaltSource getSaltSource() {
		return saltSource;
	}

	public void setSaltSource(SaltSource saltSource) {
		this.saltSource = saltSource;
	}
}

