package hu.elte.msc.webtech.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;


/**
 * The persistent class for the FINANCIAL database table.
 * 
 */
@Entity
public class Financial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator="gen")  
	@GenericGenerator(
		name="gen", 
		strategy="foreign", 
		parameters=@Parameter(name="property", value="user")
	)
	private String username;

	@Column(name="ACC_NUMBER")
	private String accNumber;

	@Column(name="VAT_NUMBER")
	private String vatNumber;
	
	@OneToOne  
    @PrimaryKeyJoinColumn
	private User user;

	public Financial() {
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAccNumber() {
		return this.accNumber;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}

	public String getVatNumber() {
		return this.vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}