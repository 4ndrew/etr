package hu.elte.msc.webtech.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The persistent class for the group_authorities database table.
 * 
 */
@Entity
@Table(name="group_authorities")
public class GroupAuthority implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;

	private String authority;

	//bi-directional many-to-one association to Group
    @ManyToOne
	private Group group;

    public GroupAuthority() {
    }

	public String getAuthority() {
		return this.authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Group getGroup() {
		return this.group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}