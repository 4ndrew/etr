package hu.elte.msc.webtech.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import java.util.Date;


/**
 * The persistent class for the PERSONAL database table.
 * 
 */
@Entity
public class Personal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator="gen")  
	@GenericGenerator(
		name="gen", 
		strategy="foreign", 
		parameters=@Parameter(name="property", value="user")
	)
	private String username;

	private String address;

	@Temporal(TemporalType.DATE)
	@Column(name="BIRTH_DATE")
	private Date birthDate;

	private String eha;

	private String email;

	@Column(name="ID_NUMBER")
	private String idNumber;

	@Column(name="MOTHERS_NAME")
	private String mothersName;

	private String name;
	
	@OneToOne  
    @PrimaryKeyJoinColumn
	private User user;

	public Personal() {
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getEha() {
		return this.eha;
	}

	public void setEha(String eha) {
		this.eha = eha;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIdNumber() {
		return this.idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getMothersName() {
		return this.mothersName;
	}

	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}