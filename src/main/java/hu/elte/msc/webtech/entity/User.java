package hu.elte.msc.webtech.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String username;

	private byte enabled;

	private String password;

	private String salt;

	//bi-directional many-to-one association to Authority
	@OneToMany(mappedBy="user")
	@Cascade(value = {CascadeType.ALL})
	private List<Authority> authorities;
	
	@ManyToMany(
	        targetEntity=hu.elte.msc.webtech.entity.Cours.class
	)
	@Cascade(value = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(
	        name="USERSCOURSES",
	        joinColumns=@JoinColumn(name="USERS_USERNAME"),
	        inverseJoinColumns=@JoinColumn(name="COURSES_CODE")
	)
	private List<Cours> courses;
	
	@OneToOne(mappedBy="user")
	@Cascade(value = {CascadeType.ALL})
	private Personal personal;
	
	@OneToOne(mappedBy="user")
	@Cascade(value = {CascadeType.ALL})
	private Financial financial;

    public User() {
    }

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public byte getEnabled() {
		return this.enabled;
	}

	public void setEnabled(byte enabled) {
		this.enabled = enabled;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return this.salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public List<Authority> getAuthorities() {
		return this.authorities;
	}

	public void setAuthorities(List<Authority> authorities) {
		this.authorities = authorities;
	}

	public List<Cours> getCourses() {
		return courses;
	}

	public void setCourses(List<Cours> courses) {
		this.courses = courses;
	}

	public Personal getPersonal() {
		return personal;
	}

	public void setPersonal(Personal personal) {
		this.personal = personal;
	}

	public Financial getFinancial() {
		return financial;
	}

	public void setFinancial(Financial financial) {
		this.financial = financial;
	}
	
}