package hu.elte.msc.webtech.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the COURSES database table.
 * 
 */
@Entity
@Table(name="COURSES")
public class Cours implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String code;

	private BigDecimal credit;

	private String description;

	private String name;

	//bi-directional many-to-one association to Userscours
//	@OneToMany(mappedBy="cours")
	@ManyToMany(
	        mappedBy = "courses",
	        targetEntity = User.class
	)
	@Cascade(value = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<User> users;

	public Cours() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public BigDecimal getCredit() {
		return this.credit;
	}

	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "Cours [code=" + code + ", credit=" + credit + ", description="
				+ description + ", name=" + name + ", users=" + users + "]";
	}
}