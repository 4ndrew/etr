package hu.elte.msc.webtech.service;

import hu.elte.msc.webtech.entity.Cours;

import java.util.List;

public interface GuestService {
	
	public List<Cours> getCourses(int pageNo, int pageSize);
}
