package hu.elte.msc.webtech.service;

import hu.elte.msc.webtech.entity.User;

import org.springframework.validation.Errors;

public interface RegistrationService {
	
	User initUser(User user);
	void save(User regensUser);
	void validate(Object target, Errors errors);
	boolean isUserAdmin(String userId);
	void merge(User user);

}