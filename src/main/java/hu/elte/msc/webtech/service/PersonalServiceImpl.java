package hu.elte.msc.webtech.service;

import hu.elte.msc.webtech.dao.UserDao;
import hu.elte.msc.webtech.entity.User;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PersonalServiceImpl implements PersonalService {
	
	@Autowired
	UserDao userDao;

	@Override
	public User getUserByUsername(String username) {
		
		return userDao.getUserById(username);
	}
	
	@Override
	@RolesAllowed("ROLE_ADMIN")
	public User getSelectedUser(String userName){
		
		return getUserByUsername(userName);
	}

	@Override
	public void mergeUser(User user) {
		userDao.merge(user);
	}
	
	@Override
	public void saveOrUpdate(User user) {
		userDao.saveOrUpdate(user);
	}
	
	@Override
	public List<User> getUsers(){
		return userDao.getUsers();
	}
	
	@Override
	public Map<String,String> getUsersForSelect(){
		
		List<User> userList = getUsers();
		Map<String,String> userSelectList = new LinkedHashMap<String,String>();
		
		userSelectList.put("","");
		
		for (User user : userList) {
			userSelectList.put(user.getUsername(), user.getPersonal().getEha());
		}
		
		return userSelectList;
	}
}
