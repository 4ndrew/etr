package hu.elte.msc.webtech.service;

import hu.elte.msc.webtech.dao.UserDao;
import hu.elte.msc.webtech.entity.Authority;
import hu.elte.msc.webtech.entity.User;
import hu.elte.msc.webtech.security.DatabasePasswordSecurerBean;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

@Service
public class RegistrationServiceImpl implements RegistrationService {
	
	@Autowired
	UserDao userDao;
	@Autowired
	DatabasePasswordSecurerBean sercurer;

	@Override
	public User initUser(User regensUser) {
		
		List<Authority> authorities = new ArrayList<Authority>();
		Authority au = new Authority();
		au.setAuthority("ROLE_USER");
		au.setUser(regensUser);
		authorities.add(au);
		regensUser.setAuthorities(authorities);
		try {
			regensUser.setSalt("S"+generateSalt());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return regensUser;
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub

	}

	@Override
	public void save(User user) {
		
		userDao.save(user);
		sercurer.secureUser(user);
		userDao.update(user); 
	}
	
	@Override
	public void merge(User user) {
		
		userDao.merge(user); 
	}
	
	private String generateSalt() throws NoSuchAlgorithmException {
	    SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
	    byte [] bytes = new byte[12];
	    random.nextBytes(bytes);
	    return byteArrayToHex(bytes);
	}

	private String byteArrayToHex(byte[] bytes) {
	    StringBuffer sb = new StringBuffer();
	    for (int i = 0; i < bytes.length; i++) {
	 String theHex = Integer.toHexString(bytes[i] & 0xFF).toUpperCase();
	 sb.append(theHex.length() == 1 ? "0" + theHex : theHex);
	    }
	    return sb.toString();
	}

    @Override
    @Transactional
    public boolean isUserAdmin(String userId) {
        User user = userDao.getUserById(userId);
        
        for(Authority au: user.getAuthorities()){
            if(au.getAuthority().toLowerCase().equals("role_admin"))
                return true;
        }
        return false;
    }
}
