package hu.elte.msc.webtech.service;

import hu.elte.msc.webtech.dao.UserDao;
import hu.elte.msc.webtech.entity.Cours;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GuestServiceImpl implements GuestService {
	
	@Autowired
	UserDao userDao;

	@Override
	@Transactional
	public List<Cours> getCourses(int pageNo, int pageSize) {
		return userDao.getCoursesPaginated(pageNo, pageSize);
	}

}
