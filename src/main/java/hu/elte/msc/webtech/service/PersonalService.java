package hu.elte.msc.webtech.service;

import java.util.List;
import java.util.Map;

import hu.elte.msc.webtech.entity.User;

public interface PersonalService {
	
	public User getUserByUsername(String username);
	public User getSelectedUser(String userName);
	public void mergeUser(User user);
	public void saveOrUpdate(User user);
	public List<User> getUsers();
	public Map<String, String> getUsersForSelect();

}
