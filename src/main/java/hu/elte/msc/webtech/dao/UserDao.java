package hu.elte.msc.webtech.dao;

import hu.elte.msc.webtech.entity.Cours;
import hu.elte.msc.webtech.entity.User;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class UserDao extends AbstractDao{
	
	public void save(User user) {
       
		getCurrentSession().save(user);
    
	}
	
	public void update(User user) {
	       
		getCurrentSession().update(user);
    
	}
	
	public void saveOrUpdate(User user) {
	       
		getCurrentSession().saveOrUpdate(user);
    
	}
	
	public void merge(User user) {
	       
		getCurrentSession().merge(user);
    
	}
	
    public User getUserById(String user) {

        Query q = createQuery("from User r where r.username = :usr");
        q.setString("usr", user);
        
        return (User) q.uniqueResult();
    }
    
    @SuppressWarnings("unchecked")
	public List<User> getUsers() {
    	
    	/*
    	SELECT u.USERNAME,count(AU.AUTHORITY)
		FROM USERS U
		INNER JOIN AUTHORITIES AU
		ON U.USERNAME=AU.USERNAME
		GROUP BY U.USERNAME
		HAVING COUNT(AU.AUTHORITY) = 1;
    	 */
    	
    	Query q = getCurrentSession().createSQLQuery(
    			"SELECT U.USERNAME, U.ENABLED, U.PASSWORD, U.SALT " +
    			"FROM USERS U " +
    			"INNER JOIN AUTHORITIES AU " +
    			"ON U.USERNAME=AU.USERNAME " +
    			"GROUP BY U.USERNAME, U.ENABLED, U.PASSWORD, U.SALT " +
    			"HAVING COUNT(AU.AUTHORITY) = 1")
    			.addEntity(User.class);
    			
    	return (List<User>)q.list();
    	
    }
    
    @SuppressWarnings("unchecked")
	public List<Cours> getCoursesPaginated(int pageNo, int pageSize) {
    	
    	/*
	    	 select *
			 from (
			SELECT 
			  c.CODE,c.NAME,c.CREDIT,c.DESCRIPTION,
			  ROW_NUMBER() 
			  OVER (ORDER BY c.CODE) RN
			 FROM COURSES c )
			WHERE RN BETWEEN 20 AND 30 
			order by rn;
		*/
    	
    	Query q = getCurrentSession().createSQLQuery(
    			"select CODE,Credit,DESCRIPTION,Name from ( " +
    			"SELECT " +
    			" c.CODE,c.Credit,c.DESCRIPTION,c.Name, " +
    			"ROW_NUMBER() " +
    			"OVER (ORDER BY c.CODE) RN " +
    			"FROM COURSES c ) " +
    			"WHERE RN BETWEEN :pos1 AND :pos2 " +
    			"order by rn"
    			)
    			.addEntity(Cours.class)
    			.setParameter("pos1", ((pageNo-1) * pageSize) + 1)
    			.setParameter("pos2", (pageNo) * pageSize);
    	
    	return (List<Cours>)q.list();
    	
    }
	
}
