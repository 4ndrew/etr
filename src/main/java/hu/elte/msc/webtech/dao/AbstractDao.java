package hu.elte.msc.webtech.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public abstract class AbstractDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * Aktualis adatbazis kapcsolat lekerese
	 * 
	 * @return
	 */
	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	/**
	 * 
	 * @param hql
	 * @return
	 */
	protected Query createQuery(String hql) {
		return getCurrentSession().createQuery(hql);
	}
	
	/**
	 * 
	 * @param entity
	 */
	protected void saveOrUpdate(Object entity) {
		getCurrentSession().saveOrUpdate(entity);
	}

}
