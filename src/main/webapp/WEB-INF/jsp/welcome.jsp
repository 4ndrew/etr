<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="subpage_header.jsp"/>

	<p class=title><spring:message code="subpage.main"/></p>
	
	<h4>Feladatleírás</h4>
	<ol>
		<b><li>Az alkalmazás</li></b>
		<ul>
			<li>Beadandónk célja egy webes alapú Egyszerűsített Tanulmány Rendszer megvalósítása, amely hallgatók adatainak karbantartását valamint tárgyfelvételt valósít meg.</li>
		</ul>
		<br>
		<b><li>Felhasznált technológiák</li></b>
		Szerver oldali technológiák:
		<ul>
			<li>Java</li>
			<li>Spring Framework</li>
			<li>JSP</li>
			<li>Oracle RDBMS 11g2</li>
			<li>Hibernate</li>
			<li>Maven</li>
		</ul>
		Kliens oldali technológiák:
		<ul>
			<li>HTML5</li>
			<li>JavaScript</li>
			<li>jQuery</li>
			<li>CSS3</li>
		</ul>
	</ol>
	
<jsp:include page="subpage_footer.jsp"/>
