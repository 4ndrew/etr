<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
 <!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style.css" />
</head>
<body onload='document.f.j_username.focus();'>
	<div id ="content" class="login_content">
		<span style="float: left">
	    <a href="?lang=en">en</a> 
	    | 
	    <a href="?lang=hu">hu</a>
		</span>
		<br/>
		<p><label class="header"><spring:message code="login.elte"/></label></p>
		<div class="logo_container">
			<div class="logo_img"><img alt="Logo" src="<%=request.getContextPath()%>/img/logo.png" width="50" height="50"/></div>
			<div class="logo_text"><spring:message code="login.etr"/></div>
		</div>
		<h3><label class="header"><spring:message code="login.header"/></label></h3>
	
		<c:if test="${not empty error}">
			<div class="errorblock">
				<spring:message code="login.unsuccessful"/><br /><spring:message code="login.unsuccessful.cause"/>
				${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
			</div>
		</c:if>
	
		<form:form name='f' action="j_spring_security_check" method='POST'>
	
			<table>
				<tr>
					<td><label><spring:message code="login.user"/></label>
					<input type='text' name='j_username' value='' class='k-input' >
					</td>
				</tr>
				<tr>
					<td><label><spring:message code="login.password"/></label>
					<input type='password' name='j_password' class='k-input' />
					</td>
				</tr>
				<tr>
					<td>
					<input name="submit" type="submit" class="k-button"
						   value="<spring:message code="login.submit"/>" />
					<input name="reset" type="reset" class="k-button"
						value="<spring:message code="login.reset"/>" />
					<div class="guest">
						<a  href="<%=request.getContextPath()%>/guest.html"><spring:message code="login.guest"/></a>
					</div>
					</td>
				</tr>
			</table>
		</form:form >
	</div>
	<img id="elte_ik_21" alt="Egyetem" src="<%=request.getContextPath()%>/img/footer_bar.png"/>
	<footer>
	  <jsp:include page="footer.jsp"/>
	</footer>
</body>
</html>