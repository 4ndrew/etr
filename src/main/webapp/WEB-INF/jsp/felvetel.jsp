<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Tanuló Felvétele</title>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style.css" />
</head>
<body>
	<form:form id="tanuloHozzaasasFrm" action="welcome.html" commandName="tanulo" >
		<div class="frame">
			<p class="title">Add student</p>
            <table>
                <tr>
                    <td><form:hidden path="id" /></td>
                </tr>
                <tr>
                    <td>
                    	<label>Name <span class="required">*</span></label>
                    	<form:input path="nev" type='text' class='k-input' required='true' />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<label>EHA <span class="required">*</span></label>
                    	<form:input path="eha" type='text' class='k-input' required='true'  />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<label>Email <span class="required">*</span></label>
                    	<form:input path="email" type='text' class='k-input' required='true' />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<label>Date of birth <span class="required">*</span></label>
                    	<input type="date" class='k-input' name="szulDatum" id="szulDatum" value="${tanulo.szulDatum}" required='true' />
                    </td>
                </tr>
                <tr>
                    <td><input type="submit" value="Submit" class="k-button"/></td>
                </tr>
           	 </table>
        </div>
        </form:form>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/etr.js"></script>
</body>
</html>