<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="subpage_header.jsp"/>

	<p class=title>List courses</p>
	
	<body>
	<table id="lista">
			<!-- Table header -->
			<thead>
				<tr>
					<th>Kód</th>
					<th>Kredit</th>
					<th>Tárgynév</th>
				</tr>
			</thead>
			<!-- Table footer -->
			<tfoot>
			</tfoot>
			<!-- Table body -->
			<tbody>
				<c:forEach var="course" items="${courses}" varStatus="loopStatus">
					<tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
						<td>${course.code}</td>
						<td>${course.credit}</td>
						<td>${course.name}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<a  href="<%=request.getContextPath()%>/welcome.html"><spring:message code="guest.back"/></a>
</body>
	
<jsp:include page="subpage_footer.jsp"/>
