<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Welcome Page</title>

<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/style.css" />

</head>

<body>
	<div class="frame">
		<label class="header" id="welcome_header"><spring:message code="login.elte"/></label>
		<label class="username"><spring:message code="user.username"/>${userName}</label>
		<c:if test="${!roleAdmin}">
			<label><spring:message code="user.role"/><spring:message code="user.role.user"/></label>
		</c:if>
		<c:if test="${roleAdmin}">
			<label><spring:message code="user.role"/><spring:message code="user.role.admin"/></label>
		</c:if>
		<hr style="height:2px;">
		<input type="button" class="s-button"
			value="<spring:message code="subpage.personal"/>"
			onclick="window.location.href = 'personal.html'" />
		<input type="button" class="s-button"
			value="<spring:message code="subpage.listCourses"/>"
			onclick="window.location.href = 'listCourses.html'" />
		<input type="button" class="s-button"
			value="<spring:message code="subpage.CourseSignUp"/>"
			onclick="window.location.href = 'courseSignUp.html'" />
		<c:url value="/j_spring_security_logout" var="logoutUrl"/>
		<input type="button" class="s-button"
			value="<spring:message code="subpage.logout"/>"
			onclick="window.location.href = '${logoutUrl}'" />
	