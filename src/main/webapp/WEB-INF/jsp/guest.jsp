<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Welcome Page</title>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style.css" />
</head>

<body>
	<h3><label class="header"><spring:message code="guest.header"/></label></h3>
	<table id="lista">
			<!-- Table header -->
			<thead>
				<tr>
					<th>Kód</th>
					<th>Kredit</th>
					<th>Tárgynév</th>
				</tr>
			</thead>
			<!-- Table footer -->
			<tfoot>
			</tfoot>
			<!-- Table body -->
			<tbody>
				<c:forEach var="course" items="${courses}" varStatus="loopStatus">
					<tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
						<td>${course.code}</td>
						<td>${course.credit}</td>
						<td>${course.name}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<a  href="<%=request.getContextPath()%>/"><spring:message code="guest.back"/></a>
	<footer>
	  <jsp:include page="footer.jsp"/>
	</footer>
</body>
</html>