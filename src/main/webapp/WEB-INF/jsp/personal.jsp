<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jsp:include page="subpage_header.jsp"/>

	<p class=title>Personal</p>
	<form:form id="editPersonalData" action="personal.html" commandName="user" >
		<div class="frame_personal">
            <table>	
            	<c:if test="${roleAdmin}">
            	<tr>	
            		<td>
                    	<label>Select EHA to edit student <span class="required">*</span></label>
						<form:select id="selectEha" path="username"  items="${users}" />
					</td>
				</tr>
				</c:if>
				<c:if test="${!roleAdmin}">
					<tr>
	                	<td><form:hidden path="username" /></td>
	                </tr>
                </c:if>
                <tr>
                    <td>
                    	<label>Name <span class="required">*</span></label>
                    	<form:input path="personal.name" type='text' class='k-input' required='true' readonly="${readOnly}" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<label>EHA <span class="required">*</span></label>
                    	<form:input path="personal.eha" type='text' class='k-input' required='true' readonly="${readOnly}" />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<label>Email <span class="required">*</span></label>
                    	<form:input path="personal.email" type='text' class='k-input' required='true' />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<label>Date of birth <span class="required">*</span></label>
                    	<form:input type="date" class='k-input' path="personal.birthDate" name="personal.birthDate" readonly="${readOnly}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                    	<label>Mother's name <span class="required">*</span></label>
                    	<form:input path="personal.mothersName" type='text' class='k-input' required='true' readonly="${readOnly}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                    	<label>ID Number <span class="required">*</span></label>
                    	<form:input path="personal.idNumber" type='text' class='k-input' required='true' readonly="${readOnly}"/>
                    </td>
                </tr>
           	 </table>
           	 <hr style="height:2px;">
           	 <p class="title">Financial</p>
           	 <table>
           	 	<tr>
                    <td><form:hidden path="financial.username" /></td>
                </tr>
                <tr>
                    <td>
                    	<label>Account Number <span class="required">*</span></label>
                    	<form:input path="financial.accNumber" type='text' class='k-input' required='true' />
                    </td>
                </tr>
                <tr>
                    <td>
                    	<label>VAT Number <span class="required">*</span></label>
                    	<form:input path="financial.vatNumber" type='text' class='k-input' required='true' readonly="${readOnly}" />
                    </td>
                </tr>
                <tr>
                    <td><input id="updatePersonal" type="submit" value="Update" class="k-button"/></td>
                </tr>
           	 </table>
           	 <a  href="<%=request.getContextPath()%>/welcome.html"><spring:message code="guest.back"/></a>
        </div>
        </form:form>
<jsp:include page="subpage_footer.jsp"/>
