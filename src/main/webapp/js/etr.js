$('input.k-input').focus(function(){
	if(!$(this).attr('readonly')){
		$(this).css('background', 'white');
	}
});

$('input.k-input').blur(function(){
	if(!$(this).attr('readonly')){
		$(this).css('background', '#F5F5F5');
	}
});


$(function () {
	console.log("Lock and load...");
	
	var changeHandler = function (e) {
		var $this = $(this);
		if($this.val() === ''){
			$('#updatePersonal').attr('disabled', 'disabled');
		}else{
			$('#updatePersonal').removeAttr('disabled');
		}
		window.location.href='personal.html?username='+$('#selectEha').val();
	};
	
//	var queryEvent = function (e) {
//		var $this = $(this);
//		
//		window.location.href='personal.html?username='+$('#selectEha').val();
//	};

	$('#selectEha').on('change', changeHandler);
//	$('#queryPersonal').on('click', queryEvent);
	
	if($('#selectEha').val() === ''){
		$('#updatePersonal').attr('disabled', 'disabled');
	}else{
		$('#updatePersonal').removeAttr('disabled');
	}
});