/*
DROP TABLE UsersCourses CASCADE CONSTRAINTS;
DROP TABLE authorities CASCADE CONSTRAINTS;
DROP TABLE courses CASCADE CONSTRAINTS;
DROP TABLE financial CASCADE CONSTRAINTS;
DROP TABLE group_authorities CASCADE CONSTRAINTS;
DROP TABLE group_members CASCADE CONSTRAINTS;
DROP TABLE groups CASCADE CONSTRAINTS;
DROP TABLE personal CASCADE CONSTRAINTS;
DROP TABLE users CASCADE CONSTRAINTS;
*/

CREATE TABLE UsersCourses 
    ( 
     courses_code VARCHAR2 (24 CHAR)  NOT NULL , 
     users_username VARCHAR2 (50 CHAR)  NOT NULL 
    ) 
;



ALTER TABLE UsersCourses 
    ADD CONSTRAINT UsersCourses__IDX PRIMARY KEY ( courses_code, users_username ) ;


CREATE TABLE authorities 
    ( 
     username VARCHAR2 (50 CHAR) , 
     authority VARCHAR2 (50 CHAR)  NOT NULL 
    ) 
;




CREATE TABLE courses 
    ( 
     code VARCHAR2 (24 CHAR)  NOT NULL , 
     name VARCHAR2 (150 CHAR)  NOT NULL , 
     credit NUMBER  NOT NULL , 
     description VARCHAR2 (500 CHAR) 
    ) 
;



ALTER TABLE courses 
    ADD CONSTRAINT "classes PK" PRIMARY KEY ( code ) ;


CREATE TABLE financial 
    ( 
     username VARCHAR2 (50 CHAR)  NOT NULL , 
     acc_number VARCHAR2 (26 CHAR) , 
     vat_number VARCHAR2 (10 CHAR) 
    ) 
;



ALTER TABLE financial 
    ADD CONSTRAINT "financial PK" PRIMARY KEY ( username ) ;


CREATE TABLE group_authorities 
    ( 
     group_id INTEGER  NOT NULL , 
     authority VARCHAR2 (50 CHAR)  NOT NULL 
    ) 
;



ALTER TABLE group_authorities 
    ADD CONSTRAINT "group_authorities PK" PRIMARY KEY ( group_id, authority ) ;


CREATE TABLE group_members 
    ( 
     id INTEGER  NOT NULL , 
     username VARCHAR2 (50 CHAR) , 
     group_id INTEGER 
    ) 
;



ALTER TABLE group_members 
    ADD CONSTRAINT "group_members PK" PRIMARY KEY ( id ) ;


CREATE TABLE groups 
    ( 
     group_id INTEGER  NOT NULL , 
     group_name VARCHAR2 (50 CHAR)  NOT NULL 
    ) 
;



ALTER TABLE groups 
    ADD CONSTRAINT "groups PK" PRIMARY KEY ( group_id ) ;


CREATE TABLE personal 
    ( 
     username VARCHAR2 (50 CHAR)  NOT NULL , 
     name VARCHAR2 (150 CHAR)  NOT NULL , 
     eha VARCHAR2 (7 CHAR)  NOT NULL , 
     birth_date DATE  NOT NULL , 
     email VARCHAR2 (50 CHAR) , 
     id_number VARCHAR2 (8 CHAR)  NOT NULL , 
     mothers_name VARCHAR2 (150 CHAR) , 
     address VARCHAR2 (200 CHAR) 
    ) 
;



ALTER TABLE personal 
    ADD CONSTRAINT "personal PK" PRIMARY KEY ( username ) ;


CREATE TABLE users 
    ( 
     username VARCHAR2 (50 CHAR)  NOT NULL , 
     password VARCHAR2 (50 CHAR) , 
     enabled CHAR (1) , 
     salt VARCHAR2 (25)  NOT NULL 
    ) 
;



ALTER TABLE users 
    ADD CONSTRAINT "users PK" PRIMARY KEY ( username ) ;



ALTER TABLE UsersCourses 
    ADD CONSTRAINT FK_ASS_4 FOREIGN KEY 
    ( 
     courses_code
    ) 
    REFERENCES courses 
    ( 
     code
    ) 
    ON DELETE CASCADE 
;


ALTER TABLE UsersCourses 
    ADD CONSTRAINT FK_ASS_5 FOREIGN KEY 
    ( 
     users_username
    ) 
    REFERENCES users 
    ( 
     username
    ) 
    ON DELETE CASCADE 
;


ALTER TABLE group_authorities 
    ADD CONSTRAINT GroupsGroupAuthorities FOREIGN KEY 
    ( 
     group_id
    ) 
    REFERENCES groups 
    ( 
     group_id
    ) 
    ON DELETE CASCADE 
;


ALTER TABLE group_members 
    ADD CONSTRAINT GroupsGroupMembers FOREIGN KEY 
    ( 
     group_id
    ) 
    REFERENCES groups 
    ( 
     group_id
    ) 
    ON DELETE SET NULL 
;


ALTER TABLE authorities 
    ADD CONSTRAINT UsersAuthorities FOREIGN KEY 
    ( 
     username
    ) 
    REFERENCES users 
    ( 
     username
    ) 
    ON DELETE SET NULL 
;


ALTER TABLE financial 
    ADD CONSTRAINT UsersFinancial FOREIGN KEY 
    ( 
     username
    ) 
    REFERENCES users 
    ( 
     username
    ) 
    ON DELETE CASCADE 
;


ALTER TABLE group_members 
    ADD CONSTRAINT UsersGroupMembers FOREIGN KEY 
    ( 
     username
    ) 
    REFERENCES users 
    ( 
     username
    ) 
    ON DELETE SET NULL 
;


ALTER TABLE personal 
    ADD CONSTRAINT UsersStudents FOREIGN KEY 
    ( 
     username
    ) 
    REFERENCES users 
    ( 
     username
    ) 
    ON DELETE CASCADE 
;


CREATE SEQUENCE USER_SEQ
  START WITH 1
  INCREMENT BY 1
  CACHE 100;

